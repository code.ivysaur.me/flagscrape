# flagscrape

![](https://img.shields.io/badge/written%20in-PHP-blue)

A script to scrape all submissions for New Zealand's 2015 flag referendum.

Tags: scraper


## Download

- [⬇️ flagscrape.zip](dist-archive/flagscrape.zip) *(2.04 KiB)*
